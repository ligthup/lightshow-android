package com.example.ssh_with_service;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

import yuku.ambilwarna.AmbilWarnaDialog;

public class SSH_Connection extends Activity implements View.OnClickListener  {

    private Button button2;
    private Button button3;
    private Button button4;
    private TextView textView;
    private ProgressBar progressBar;
    private SeekBar seekBar;
    int mDefaultColor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zweite);

        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);


        mDefaultColor = ContextCompat.getColor(SSH_Connection.this, R.color.colorPrimary);
        button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openColorPicker();
            }
        });

        textView = (TextView) findViewById(R.id.textView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        seekBar = (SeekBar) findViewById(R.id.seekBar);


        new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {                     //Input username/password/IP-Adresse
                try {
                    executeRemoteCommand("pi", "1234", "192.168.2.171", 22);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(1);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressBar.setProgress(progress);
                textView.setText("" + progress + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // String die_eingabe;                   //Beispiel um String von anderer Aktivity zu übergeben bzw auszulesen
        // TextView eingaben_view;
        //eingabe_view = (TextView) findViewById(R.id.textView);
        // Bundle pickupData = getIntent().getExtras();
        //die_eingabe = pickupData.getString(key:"user eingabe");  //string ausgeben
        //eingabe_view.setText(die_eingabe);

        //Button button1 = findViewById(R.id.button1);
        // Button button2 = findViewById(R.id.button2);

        // button1.setOnClickListener(this);
        //button2.setOnClickListener(this);

       /*button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openActivity2();
            }
        });*/

/*
        new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    executeRemoteCommand("pi", "1234", "192.168.2.171", 22);
                    //executeRemoteCommandOff("pi", "1234", "192.168.2.171", 22);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(1);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                try {
                    executeRemoteCommand("pi", "1234", "192.168.2.171", 22);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                try {
                    executeRemoteCommandOff("pi", "1234", "192.168.2.171", 22);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }); */
    }
    public String executeRemoteCommand(String username, String password, String hostname, int port)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);    //open a new Session
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        session.connect();


        // SSH Channel
        ChannelExec channelssh = (ChannelExec)
                session.openChannel("exec");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);


        // Execute command
        channelssh.setCommand("sudo python3 neopixel_zero.py ");
        //channelssh.setCommand("sudo python3 neopixel1.py");
        //channelssh.setCommand("sudo rm test04.txt test3.txt");

        channelssh.connect();
        channelssh.disconnect();

        return baos.toString();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button2:
                Toast.makeText(this, "Button 1 clicked", Toast.LENGTH_SHORT).show(); //zeigt kurze Textmassage an dass Button geklickt wurde.
                Intent intent = new Intent(this, Buttons.class);
                startActivity(intent);
               // TextView text = (TextView) findViewById(R.id.textView);
                //text.setText(("LED_ON!"));
                break;
            case R.id.button3:
                Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                break;

        }

    }

/*
    public String executeRemoteCommand(String username, String password, String hostname, int port)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        session.connect();

        // SSH Channel
        ChannelExec channelssh = (ChannelExec)
                session.openChannel("exec");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);


        // Execute command
        // channelssh.setCommand("sudo python3 neopixel_zero.py ");
        channelssh.setCommand("sudo python3 neopixel1.py");
        //channelssh.setCommand("sudo rm test04.txt test3.txt");

        channelssh.connect();
        channelssh.disconnect();

        return baos.toString();
    }

    public String executeRemoteCommandOff(String username, String password, String hostname, int port)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        session.connect();

        // SSH Channel
        ChannelExec channelssh = (ChannelExec)
                session.openChannel("exec");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);


        // Execute command
        channelssh.setCommand("sudo python3 neopixel_zero.py ");
        //channelssh.setCommand("sudo python3 neopixel1.py");
        //channelssh.setCommand("sudo rm test04.txt test3.txt");

        channelssh.connect();
        channelssh.disconnect();

        return baos.toString();*/

        public void openColorPicker ()
        {                           //AmbilWarnaDialog means: pick a Color
            AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, mDefaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                @Override
                public void onCancel(AmbilWarnaDialog dialog) {

                }

                @Override
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    mDefaultColor = color; //Color which we choosed the privious time
                    //mLayout.setBackgroundColor(mDefaultColor); //set the Backgroundcolor in the color which we picked up in he Coolorpicker
                }
            });
            colorPicker.show();
        }

}
