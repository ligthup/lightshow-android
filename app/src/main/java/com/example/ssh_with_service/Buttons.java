package com.example.ssh_with_service;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

public class Buttons extends Activity implements View.OnClickListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button2 = findViewById(R.id.button2);
        Button button3 = findViewById(R.id.button3);

        button2.setOnClickListener(this);
        button3.setOnClickListener(this);

       /* button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openActivity2();
            }
        });*/

        new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    executeRemoteCommand("pi", "1234", "192.168.2.171", 22);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(1);
    }


    public String executeRemoteCommand(String username, String password, String hostname, int port)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        session.connect();

        // SSH Channel
        ChannelExec channelssh = (ChannelExec)
                session.openChannel("exec");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);


        // Execute command
        //channelssh.setCommand("sudo python3 neopixel_zero.py ");
        channelssh.setCommand("sudo python3 neopixel1.py");
        // channelssh.setCommand("sudo rm test04.txt test3.txt");

        channelssh.connect();
        channelssh.disconnect();

        return baos.toString();
    }

    @Override
    public void onClick(View v) {

          switch (v.getId()) {
            /*case R.id.button1:
                Toast.makeText(this, "Button 1 clicked", Toast.LENGTH_SHORT).show(); //zeigt kurze Textmassage an dass Button geklickt wurde.
                TextView text = (TextView) findViewById(R.id.textView);
                text.setText(("LED_ON!!!"));

                break;*/
            case R.id.button3:
                Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
               // TextView text1 = (TextView) findViewById(R.id.textView);
               // text1.setText(("LED_OFF!!!"));
                Intent intent = new Intent (this,SSH_Connection.class);
                startActivity(intent);
                break;

        }
    }


}
