# Android setup

## Enable Developer options & USB Debugging 

Enabling the Dev Options & USB Debugging is device specific for more info see:
https://developer.android.com/studio/debug/dev-options#enable

## Wifi Connection 

Connect your Android device to the same (Wifi-)Network as your Raspberry.

This may probably only work in a private Network, not in a public one such as in café'